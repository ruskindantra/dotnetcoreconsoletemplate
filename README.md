# Template for .net core console application
The application has the following features
1. Uses `HostBuilder` to initialize the application
1. Runs the application using an `IHostedService`
1. Uses `Serilog` to log to:
    1. ColoredConsole
    1. Initialized via `appsettings.json`
    1. Logs to a local elastic cluster
1. Uses built-in dependency injection coupled with `Autofac`
1. Demonstrates how to use developer secrets using a guid
1. Registers a simple settings object using `IOptions<>`
