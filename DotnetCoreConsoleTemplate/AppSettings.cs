namespace DotnetCoreConsoleTemplate
{
    public class AppSettings
    {
        public string SomeSetting { get; set; }
        public bool SomeFlag { get; set; }
    }
}