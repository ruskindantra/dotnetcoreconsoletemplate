﻿using System;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Debugging;

namespace DotnetCoreConsoleTemplate
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new HostBuilder()
               .ConfigureAppConfiguration((hostingContext, config) =>
               {   
                   SelfLog.Enable(Console.Error);
                   
                   IHostingEnvironment env = hostingContext.HostingEnvironment;
                   var aspnetCoreEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                   if (!string.IsNullOrEmpty(aspnetCoreEnv))
                       env.EnvironmentName = aspnetCoreEnv;
                   
                   if (env.IsDevelopment())
                   {
                       Log.Logger.Information("Loading user secrets");
                       config.AddUserSecrets<Program>();
                   }

                   config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                       .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                       .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                       .AddEnvironmentVariables();
               })
               .UseServiceProviderFactory(new AutofacServiceProviderFactory())
               .ConfigureContainer<ContainerBuilder>(containerBuilder =>
               {
                   // TODO: Register Autofac modules here
               })
               .ConfigureServices((hostContext, services) =>
               {
                   var appSettings = hostContext.Configuration.GetSection("AppSettings");
                   
                   services.AddOptions();
                   services.Configure<AppSettings>(appSettings);
                   services.AddSingleton<IHostedService, App>();

                   // TODO: register more services here if required
               })
               .UseSerilog((hostingContext, loggerConfiguration) =>
               {
                   loggerConfiguration.ReadFrom.Configuration(hostingContext.Configuration);
               });

            await builder.RunConsoleAsync();
        }
    }
}